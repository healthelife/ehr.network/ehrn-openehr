﻿archetype (adl_version=1.4; uid=b60b9e07-ea2b-40ff-a56c-4b520558d712)
	openEHR-EHR-EVALUATION.narcotics_use_summary.v0

concept
	[at0000]	-- Caffeine use summary
language
	original_language = <[ISO_639-1::en]>
description
	original_author = <
		["name"] = <"Dileep V S">
		["email"] = <"dileep@healthelife.in">
		["organisation"] = <"Healthelife Ventures LLP">
		["date"] = <"2017-09-13">
	>
	details = <
		["en"] = <
			language = <[ISO_639-1::en]>
			purpose = <"To record summary information about the individual's pattern of caffeine use.">
			use = <"Use to record summary information about the individual's pattern ofcaffeine consumption.

This archetype is to be used to record information about both current and previous caffeine use behaviour.

The specific scope of this archetype is on documentation about the use of all types of consumed caffeine because of the associated health risks.

The 'Per type' cluster of data elements allows for recording of specific details and episodes about each type of caffeine beverage used and can be repeated once per type. The list of caffeine beverages listed in the 'Per type' run-time name constraint identifies the caffeine beverage. This name constraint can be applied during template modelling or at run-time within a software application.">
			keywords = <"drinking", "dipsomania", "caffeine">
			misuse = <"Not to be used to record event-or period-based information about caffeine use, such as actual daily use or the average use over a specified period of time

Not to be used to record an assessment about caffeine dependence. Use specific archetypes for this purpose.">
			copyright = <"© Healthelife ventures llp">
		>
	>
	lifecycle_state = <"NotSet">
	other_contributors = <"Amund Aakerholt, Helse Stavanger, KORFOR, Norway", "Morten Aas, Oslo Universitetssykehus, Norway", "Tomas Alme, DIPS ASA, Norway", "Ole Andreas Bjordal, Webmed, Norway", "Rita Apelt, Department of Health,NT, Australia", "Vebjørn Arntzen, Oslo universitetssykehus HF, Norway (Nasjonal IKT editor)", "Koray Atalag, University of Auckland, New Zealand", "Gustavo Bacelar-Silva, Healthcare Designs, Brazil (openEHR Editor)", "Silje Ljosland Bakke, Nasjonal IKT HF, Norway (openEHR Editor)", "Marcus Baw, openGPSoC / BawMedical Ltd, United Kingdom", "Kristian Berg, Universitetssykehuset Nord Norge, Norway", "Lars Bitsch-Larsen, Haukeland University Hospital, Bergen, Norway", "Fredrik Borchsenius, Oslo universitetssykehus, Norway", "Chris Bullen, University of Auckland, New Zealand", "Fatemeh Chalabianloo, Helse Bergen, Norway", "Bjørn Christensen, Helse Bergen HF, Norway", "Stephen Chu, NEHTA, Australia", "Lisbeth Dahlhaug, Helse Midt - Norge IT, Norway", "Are Edvardsen, SKDE, Norway", "Einar Fosse, UNN HF, Norwegian Centre for Integrated Care and Telemedicine, Norway", "Hildegard Franke, freshEHR Clinical Informatics Ltd., United Kingdom", "Heather Grain, Llewelyn Grain Informatics, Australia", "Liv Grøtvedt, Folkehelseinstituttet, Norway", "Sam Heard, Ocean Informatics, Australia", "Kristian Heldal, Telemark Hospital Trust, Norway", "Jørn Henrik Vold, Helse Bergen, Avdeling for rusmedisin, Norway", "Anca Heyd, DIPS ASA, Norway", "Evelyn Hovenga, EJSH Consulting, Australia", "Tom Jarl Jakobsen, Helse Bergen, Norway", "Lars Morgan Karlsen, DIPS ASA, Norway", "Adriana Kitajima, CORE Consulting, Brazil", "Nils Kolstrup, Skansen Legekontor og Nasjonalt Senter for samhandling og telemedisin, Norway", "Ole Kristian Losvik, Losol AS, Norway", "Heather Leslie, Ocean Informatics, Australia (openEHR Editor)", "Rikard Lovstrom, Swedish Medical Association, Sweden", "Camilla Lund, Institute for Cancer Genetics and Informatics, Norway", "Hallvard Lærum, Direktoratet for e-helse, Norway", "Arne Løberg Sæter, DIPS ASA, Norway", "Siv Marie Lien, DIPS ASA, Norway", "Ian McNicoll, freshEHR Clinical Informatics, United Kingdom (openEHR Editor)", "Bjørn Næss, DIPS ASA, Norway", "Jeremy Oats, NT Health, Australia", "Andrej Orel, Marand d.o.o., Slovenia", "Anne Pauline Anderssen, Helse Nord RHF, Norway", "Rune Pedersen, Universitetssykehuset i Nord Norge, Norway", "Tanja Riise, Nasjonal IKT HF, Norway", "Rosalie Schultz, Anyinginyi Health Aboriginal Corporation, Australia", "Line Silsand, Universitetssykehuset i Nord-Norge, Norway", "Raymond Simkus, Brookswood Family Practice, Canada", "Lisbeth Sommervoll, Akershus Universitetssykehus, Norway", "Norwegian Review Summary, Nasjonal IKT HF, Norway", "Nyree Taylor, Ocean Informatics, Australia", "Jon Tysdahl, Furst medlab AS, Norway", "John Tore Valand, Helse Bergen, Norway (Nasjonal IKT editor)", "Ping-Cheng Wei, New Zealand">
	other_details = <
		["revision"] = <"0.0.1-alpha">
		["original_publisher"] = <"openEHR Foundation">
		["current_contact"] = <"Dileep V S, Healthelife Ventures LLP<dileep@healthelife.in>">
		["build_uid"] = <"52d28acf-3819-4065-8043-03415a3c7222">
		["original_namespace"] = <"org.openehr">
		["licence"] = <"This work is licensed under the Creative Commons Attribution-ShareAlike 3.0 License. To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/3.0/.">
		["references"] = <"Tobacco smoking summary, Published Archetype [Internet]. openEHR Foundation, openEHR Clinical Knowledge Manager [cited: 2016-12-08]. Available from: http://openehr.org/ckm/#showArchetype_1013.1.2466">
		["MD5-CAM-1.0.1"] = <"67F65586725AC08CEA498D45A3696BDE">
		["custodian_namespace"] = <"org.openehr">
		["custodian_organisation"] = <"openEHR Foundation">
	>

definition
	EVALUATION[at0000] matches {	-- Caffeine use summary
		data matches {
			ITEM_TREE[at0001] matches {	-- Tree
				items cardinality matches {0..*; unordered} matches {
					ELEMENT[at0089] occurrences matches {0..1} matches {	-- Overall status
						value matches {
							DV_CODED_TEXT matches {
								defining_code matches {
									[local::
									at0006, 	-- Lifetime non-user
									at0003, 	-- Current user
									at0005]	-- Former user
								}
							}
						}
					}
					ELEMENT[at0043] occurrences matches {0..1} matches {	-- Overall description
						value matches {
							DV_TEXT matches {*}
						}
					}
					ELEMENT[at0015] occurrences matches {0..1} matches {	-- Regular use commenced
						value matches {
							DV_DATE matches {*}
						}
					}
					ELEMENT[at0080] occurrences matches {0..1} matches {	-- Daily use commenced
						value matches {
							DV_DATE matches {*}
						}
					}
					CLUSTER[at0029] occurrences matches {0..*} matches {	-- Per type
						name matches {
							DV_TEXT matches {*}
						}
						items cardinality matches {1..*; unordered} matches {
							ELEMENT[at0052] occurrences matches {0..1} matches {	-- Status
								value matches {
									DV_CODED_TEXT matches {
										defining_code matches {
											[local::
											at0061, 	-- Current user
											at0059, 	-- Former user
											at0091]	-- Lifetime non-user
										}
									}
								}
							}
							ELEMENT[at0053] occurrences matches {0..1} matches {	-- Description
								value matches {
									DV_TEXT matches {*}
								}
							}
							CLUSTER[at0064] occurrences matches {0..*} matches {	-- Per episode
								items cardinality matches {1..*; unordered} matches {
									ELEMENT[at0081] occurrences matches {0..1} matches {	-- Episode label
										value matches {
											DV_COUNT matches {
												magnitude matches {|>=1|}
											}
											DV_TEXT matches {*}
										}
									}
									ELEMENT[at0013] occurrences matches {0..1} matches {	-- Episode start date
										value matches {
											DV_DATE matches {*}
										}
									}
									ELEMENT[at0082] occurrences matches {0..1} matches {	-- Episode end date
										value matches {
											DV_DATE matches {*}
										}
									}
									ELEMENT[at0030] occurrences matches {0..1} matches {	-- Pattern
										value matches {
											DV_CODED_TEXT matches {
												defining_code matches {
													[local::
													at0083, 	-- Daily
													at0084]	-- Non-daily
												}
											}
											DV_TEXT matches {*}
										}
									}
									ELEMENT[at0023] occurrences matches {0..1} matches {	-- Frequency of use
										value matches {
											C_DV_QUANTITY <
												property = <[openehr::382]>
												list = <
													["1"] = <
														units = <"1/d">
														magnitude = <|>=0.0|>
														precision = <|0|>
													>
													["2"] = <
														units = <"1/wk">
														magnitude = <|>=0.0|>
														precision = <|0|>
													>
												>
											>
										}
									}
									ELEMENT[at0025] occurrences matches {0..1} matches {	-- Number of quit attempts
										value matches {
											DV_COUNT matches {
												magnitude matches {|>=0|}
											}
										}
									}
									allow_archetype CLUSTER[at0026] occurrences matches {0..*} matches {	-- Episode details
										include
											archetype_id/value matches {/openEHR-EHR-CLUSTER\.cessation_attempts(-[a-zA-Z0-9_]+)*\.v1/}
									}
									ELEMENT[at0087] occurrences matches {0..1} matches {	-- Episode comment
										value matches {
											DV_TEXT matches {*}
										}
									}
								}
							}
							ELEMENT[at0113] occurrences matches {0..1} matches {	-- Start date
								value matches {
									DV_DATE matches {*}
								}
							}
							ELEMENT[at0014] occurrences matches {0..1} matches {	-- Quit date
								value matches {
									DV_DATE matches {*}
								}
							}
							allow_archetype CLUSTER[at0077] occurrences matches {0..*} matches {	-- Type details
								include
									archetype_id/value matches {/.*/}
							}
							ELEMENT[at0069] occurrences matches {0..1} matches {	-- Comment
								value matches {
									DV_TEXT matches {*}
								}
							}
						}
					}
					allow_archetype CLUSTER[at0086] occurrences matches {0..*} matches {	-- Overall details
						include
							archetype_id/value matches {/openEHR-EHR-CLUSTER\.change(-[a-zA-Z0-9_]+)*\.v1/}
					}
					ELEMENT[at0016] occurrences matches {0..1} matches {	-- Overall quit date
						value matches {
							DV_DATE matches {*}
						}
					}
					ELEMENT[at0019] occurrences matches {0..1} matches {	-- Overall comment
						value matches {
							DV_TEXT matches {*}
						}
					}
				}
			}
		}
		protocol matches {
			ITEM_TREE[at0021] matches {	-- Tree
				items cardinality matches {0..*; unordered} matches {
					allow_archetype CLUSTER[at0073] occurrences matches {0..*} matches {	-- Extension
						include
							archetype_id/value matches {/.*/}
					}
					ELEMENT[at0071] occurrences matches {0..*} matches {	-- Quit date definition
						value matches {
							DV_TEXT matches {*}
						}
					}
					ELEMENT[at0085] occurrences matches {0..1} matches {	-- Quit attempt definition
						value matches {
							DV_TEXT matches {*}
						}
					}
					ELEMENT[at0075] occurrences matches {0..*} matches {	-- Current drinker definition
						value matches {
							DV_TEXT matches {*}
						}
					}
					ELEMENT[at0076] occurrences matches {0..*} matches {	-- Former drinker definition
						value matches {
							DV_TEXT matches {*}
						}
					}
					ELEMENT[at0079] occurrences matches {0..*} matches {	-- Lifetime non-drinker definition
						value matches {
							DV_TEXT matches {*}
						}
					}
					ELEMENT[at0103] occurrences matches {0..1} matches {	-- Alcohol unit definition (volume)
						value matches {
							C_DV_QUANTITY <
								property = <[openehr::129]>
								list = <
									["1"] = <
										units = <"ml">
										magnitude = <|>=0.0|>
									>
									["2"] = <
										units = <"cl">
										magnitude = <|>=0.0|>
									>
									["3"] = <
										units = <"l">
										magnitude = <|>=0.0|>
									>
									["4"] = <
										units = <"[foz_us]">
										magnitude = <|>=0.0|>
									>
								>
							>
						}
					}
					ELEMENT[at0104] occurrences matches {0..1} matches {	-- Alcohol unit definition (mass)
						value matches {
							C_DV_QUANTITY <
								property = <[openehr::124]>
								list = <
									["1"] = <
										units = <"g">
										magnitude = <|>=0.0|>
										precision = <|0|>
									>
									["2"] = <
										units = <"[oz_av]">
										magnitude = <|>=0.0|>
										precision = <|1|>
									>
								>
							>
						}
					}
					ELEMENT[at0022] occurrences matches {0..1} matches {	-- Last updated
						value matches {
							DV_DATE_TIME matches {*}
						}
					}
				}
			}
		}
	}

ontology
	term_definitions = <
		["en"] = <
			items = <
				["at0000"] = <
					text = <"Caffeine use summary">
					description = <"Summary or persistent information about the caffeine use habits of an individual.">
				>
				["at0001"] = <
					text = <"Tree">
					description = <"@ internal @">
				>
				["at0003"] = <
					text = <"Current user">
					description = <"Individual is a current consumer of narcotics.">
				>
				["at0005"] = <
					text = <"Former user">
					description = <"Individual has previously consumed narcotics but is not a current user.">
				>
				["at0006"] = <
					text = <"Lifetime non-user">
					description = <"Individual has never consumed narcotics">
				>
				["at0013"] = <
					text = <"Episode start date">
					description = <"Date when this episode commenced.">
					comment = <"Can be a partial date, for example, only a year.">
				>
				["at0014"] = <
					text = <"Quit date">
					description = <"Date when the individual last consumed the specified alcoholic beverage.">
					comment = <"Can be a partial date, for example, only a year. Definitions for a 'Quit date' vary enormously and can be defined using the 'Quit data definition' data element in the Protocol section of this archetype. This date will be identical to the 'Episode end date' for the most recent episode. This date could be used by decision support guidance to determine if the  individual is at risk of relapse, for example in the first 12 months since quitting.">
				>
				["at0015"] = <
					text = <"Regular use commenced">
					description = <"The date or partial date when the individual first started frequent or regular, but usually non-daily, alcohol consumption.">
					comment = <"Can be a partial date, for example, only a year. For example, this date could represent when the individual commenced drinking every Friday night or at parties. ">
				>
				["at0016"] = <
					text = <"Overall quit date">
					description = <"The date when the individual last ceased using alcohol of any type.">
					comment = <"Can be a partial date, for example, only a year. This date could be used by decision support guidance to determine if the  individual is at risk of relapse, for example in the first 12 months since quitting.">
				>
				["at0019"] = <
					text = <"Overall comment">
					description = <"Additional narrative about all alcohol use that has not been captured in other fields.">
					comment = <"For example: stopped drinking or reduced amount on becoming pregnant.">
				>
				["at0021"] = <
					text = <"Tree">
					description = <"@ internal @">
				>
				["at0022"] = <
					text = <"Last updated">
					description = <"The date this alcohol use summary was last updated.">
				>
				["at0023"] = <
					text = <"Frequency of use">
					description = <"Estimate of number of alcohol units of the specified type of alcoholic beverage consumed.">
					comment = <"For example: the number of alcohol units per day or per week. This data element is redundant if a value is recorded for 'Typical use(mass)'.">
				>
				["at0025"] = <
					text = <"Number of quit attempts">
					description = <"Total number of times the individual has attempted to stop consuming the specified type of alcoholic beverage within this episode.">
				>
				["at0026"] = <
					text = <"Episode details">
					description = <"Additional structured details about the specified episode of alcohol use.">
				>
				["at0029"] = <
					text = <"Per type">
					description = <"Details about consumption of a specified type of alcoholic beverage.">
					comment = <"The run-time name constraint on this Cluster enables simple templates for each required type to be designed in templates, or the types to be managed exclusively at run-time. The list of names can be extended at run-time if additional types of alcoholic beverages are identified locally.">
				>
				["at0030"] = <
					text = <"Pattern">
					description = <"The typical pattern of consumption for the specified type of alcoholic beverage.">
				>
				["at0043"] = <
					text = <"Overall description">
					description = <"Narrative summary about the individual's overall alcohol use pattern and history.">
					comment = <"Use this data element to record a narrative description only where the structured data does not adequately reflect the alcohol use habits for this individual or to incorporate unstructured alcohol use information from existing or legacy clinical systems into an archetyped format.">
				>
				["at0052"] = <
					text = <"Status">
					description = <"Statement about current alcohol use behaviour for the specified type of alcoholic beverage.">
				>
				["at0053"] = <
					text = <"Description">
					description = <"Narrative summary about alcohol use behaviour for the specified type of alcoholic beverage.">
				>
				["at0059"] = <
					text = <"Former user">
					description = <"Individual has previously consumed the specified narcotics but is not a current user.">
				>
				["at0061"] = <
					text = <"Current user">
					description = <"Individual is a current consumer of the specified narcotics.">
				>
				["at0064"] = <
					text = <"Per episode">
					description = <"Details about a discrete period of consumption for the specified type of alcoholic beverage.">
				>
				["at0069"] = <
					text = <"Comment">
					description = <"Additional narrative about consuming of the specified alcoholic beverage, not captured in other fields.">
				>
				["at0071"] = <
					text = <"Quit date definition">
					description = <"The applied definition for the 'Quit date' data elements used in this archetype.">
				>
				["at0073"] = <
					text = <"Extension">
					description = <"Additional information required to capture local content or to align with other reference models/formalisms.">
					comment = <"For example: local information requirements or additional metadata to align with FHIR or CIMI equivalents.">
				>
				["at0075"] = <
					text = <"Current drinker definition">
					description = <"The applied definition for the 'Current drinker' value in each of the 'Status' data elements used in this archetype.">
				>
				["at0076"] = <
					text = <"Former drinker definition">
					description = <"The applied definition for the 'Former drinker' value in each of the 'Status' data elements used in this archetype.">
				>
				["at0077"] = <
					text = <"Type details">
					description = <"Additional structured details about the consumption of the specified alcoholic beverage.">
				>
				["at0079"] = <
					text = <"Lifetime non-drinker definition">
					description = <"The applied definition for the 'Lifetime non-drinker' value in each of the 'Status' data elements used in this archetype.">
				>
				["at0080"] = <
					text = <"Daily use commenced">
					description = <"The date or partial date when the individual first started daily alcohol consumption.">
					comment = <"Can be a partial date, for example, only a year.">
				>
				["at0081"] = <
					text = <"Episode label">
					description = <"Identification of an episode of alcohol consumption - either as a number in a sequence and/or a named event.">
					comment = <"For example: '2' as the second episode within a sequence of episodes; or 'Pregnancy with twins' if describing the alcohol consumption during a health event such as during a specific pregnancy.">
				>
				["at0082"] = <
					text = <"Episode end date">
					description = <"Date when this episode ceased.">
					comment = <"Can be a partial date, for example, only a year. This data field will be empty if the episode is current and ongoing.">
				>
				["at0083"] = <
					text = <"Daily">
					description = <"Consuming the specified alcoholic beverage at least once every day.">
				>
				["at0084"] = <
					text = <"Non-daily">
					description = <"Not consuming the specified alcoholic beverage every day.">
				>
				["at0085"] = <
					text = <"Quit attempt definition">
					description = <"The applied definition for a Quit attempt used to determine value for the 'Number of quit attempts' data element used in this archetype.">
					comment = <"For example: 'stopped consuming alcohol for one day or longer with the intention of quitting'.">
				>
				["at0086"] = <
					text = <"Overall details">
					description = <"Additional structured details about the overall alcohol use behaviour.">
				>
				["at0087"] = <
					text = <"Episode comment">
					description = <"Additional narrative about alcohol use during the specified episode, not captured in other fields.">
				>
				["at0089"] = <
					text = <"Overall status">
					description = <"Statement about current alcohol use behaviour for all types of alcohol.">
				>
				["at0091"] = <
					text = <"Lifetime non-user">
					description = <"Individual has never consumed the specified narcotics.">
				>
				["at0103"] = <
					text = <"Alcohol unit definition (volume)">
					description = <"Volume of alcohol defining an alcohol unit as used in the 'Typical use (alcohol units)' element in this archetype.">
				>
				["at0104"] = <
					text = <"Alcohol unit definition (mass)">
					description = <"Mass of alcohol defining an alcohol unit as used in the 'Typical use (alcohol units)' element in this archetype.">
				>
				["at0113"] = <
					text = <"Start date">
					description = <"*">
				>
			>
		>
	>
